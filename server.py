import cherrypy, os, datetime, sys
from jinja2 import Environment, FileSystemLoader, select_autoescape

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.pool import SingletonThreadPool
from models import Usuario, TFG 
from cp_sqlalchemy import SQLAlchemyTool, SQLAlchemyPlugin

from urllib.parse import urlparse

from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.utils import OneLogin_Saml2_Utils

from gestor_tfgs import GestorTFGs
from sso import SingleSignOn


Base = declarative_base()    
        
if __name__ == '__main__':

    # Tool de la BD
    cherrypy.tools.db = SQLAlchemyTool(expire_on_commit = False)

    # applicaton config is provided
    path = os.path.abspath(os.path.dirname(__file__))
    config={    
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(path, 'public')
        }
    }    

    cherrypy.tree.mount(GestorTFGs(), '/tfgs/', config = config)
    cherrypy.tree.mount(SingleSignOn(), '/', config = config)

    cherrypy.config.update("conf/app.cfg")
    if len(sys.argv) >= 2 and sys.argv[1] == 'prepo':
        cherrypy.config.update("conf/prepro.cfg") 
    else:
        cherrypy.config.update("conf/produccion.cfg") 
        
    # Plugin de SQLAlchemy
    sqlalchemy_plugin = SQLAlchemyPlugin(
        cherrypy.engine, Base, 'sqlite:///tfgs.db',
        echo=False
    )
    sqlalchemy_plugin.subscribe()
    sqlalchemy_plugin.create()

    cherrypy.engine.signals.subscribe()
    cherrypy.engine.start()
    cherrypy.engine.block()
