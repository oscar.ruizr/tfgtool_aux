import cherrypy, os, datetime, unittest.mock, logging
import pdb
from jinja2 import Environment, FileSystemLoader, select_autoescape

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy import or_
from models import Usuario, TFG 
from utils import check_login, check_esProfesor, enviarCorreo

logging.basicConfig(filename="debug.log", level=logging.DEBUG)

class PruebaTFGs(object):

	ACADEMIC_YEAR = "1819"    

	def __init__(self):
		self.env = Environment(
		loader=FileSystemLoader('templates'),
		autoescape=select_autoescape(['html', 'xml']))
	
	@property
	def db(self):
		return cherrypy.request.db


	@cherrypy.expose
	def index(self):

		# MODO MANTENIMIENTO
		#return self.env.get_template("mantenimiento.html").render()

		# MODO PRE-PRODUCCIÓN
		if (cherry.config['entorno'] == 'prepo'):
			raise cherrypy.HTTPRedirect("home")            

		# Comprobamos si el usuario está correctamente 
		# autenticado
		if 'logged' not in cherrypy.session:
			raise cherrypy.HTTPRedirect("login")
		else:
		# Ok, vamos al dashboard
			raise cherrypy.HTTPRedirect("home")

	@cherrypy.expose
	@cherrypy.tools.check_login()
	def home(self):                    
        
        # Si estamos en pre-producción, utilizamos un usuario fake para saltarnos
        # la autenticación SAML
		email = cherrypy.config['fake_usuario'] if cherrypy.config['entorno'] == 'prepo'\
					else cherrypy.session['samlUserdata']['cn'][0]

		if not cherrypy.config['entorno'] == 'prepo':
			logging.debug("Usuario SAML: email [{}][{}][{}][{}]".format(cherrypy.session['samlUserdata']['cn'][0],\
				cherrypy.session['samlUserdata']['nombre'][0], cherrypy.session['samlUserdata']['apellido1'][0],\
					cherrypy.session['samlUserdata']['apellido2'][0]))

        # Buscamos el usuario en la BD
		user = self.db.query(Usuario).filter(Usuario.email == email).first()
		logging.debug("Usuario encontrado: [{}][{}]".format(user.id, user.email))            

        # Si no existe, es la primera que el usuario entra en el sistema, creamos su usuario
		if user == None:
		# Algunos usuarios extranjeros no tienen definido el segundo apellido
			try:
				apellido2 = cherrypy.session['samlUserdata']['apellido2'][0]
			except IndexError:
				apellido2 = ""

            # TODO: Cada vez que se cree un usuario, hay que asignar correctamente el valor tipoUsuario
            # Creamos el objeto user y lo guardamos en la BD
		user = Usuario(email = email, nombre = cherrypy.session['samlUserdata']['nombre'][0],
				apellido1 = cherrypy.session['samlUserdata']['apellido1'][0],
				apellido2 = apellido2)

		print("LLEGAS?")
		print(user.esEstudiante())
		self.db.add(user)    
		self.db.commit()

        # Guardamos el usuario en la sesión y lo marcamos como logado
		cherrypy.session['user'] = user
		cherrypy.session['logged'] = True            
                
        # TODO: Reunir todas estas funciones de tipo query en models.TFG
        # Si es un estudiante, mostramos sólo su TFG
		if user.esEstudiante():
			tfgs = self.db.query(TFG).filter(TFG.estudiante_id == user.id).all()
		else:
			tfgs = self.db.query(TFG).filter(TFG.tutor_id == user.id).all()                        

        #print(user.TipoUsuario)
		print("ESTUDIANTE")
		print(user.esEstudiante())
		print("TUTOR")
		print(user.esTutor(tfgs[0]))
		print("PONENTE")
		print(user.esPonente(tfgs[0]))
		print("NOMBRE")
		print(user.getNombreCompleto())
		return self.env.get_template("dashboard.html").render(tfgs = tfgs, user = user)


	@cherrypy.expose
	#@cherrypy.tools.check_esProfesor()
	def solicitudDefensa(self, tfg_codigo, **kwargs):

		upload_path = os.path.dirname(__file__)
		absDir = os.path.join(os.getcwd(), upload_path)
		print(upload_path)

		if cherrypy.request.method == 'POST' and\
			'ufile' in kwargs: 
			print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa")
			print(kwargs['ufile'].file)
			#print(cherrypy.response.headers['Content-Disposition'])
			print(kwargs['ufile'].filename)
			print(cherrypy.response.body)
			upload_file = os.path.join(upload_path, kwargs['ufile'].filename)
			size = 0
			with open(upload_file, 'wb') as out:
				while True:
					data = kwargs['ufile'].file.read(8192)
					if not data:
						break

					out.write(data)
					size += len(data)
			out = ''

		if 'download' in kwargs:  
			prnt("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE")
			path = os.path.join(absDir, 'licencia.pdf')
			return static.serve_file(path, 'application/x-download', 'attachment', os.path.basename(path))
            #print(kwargs['ufile'].file.read(8192))
            #cherrypy.request['ufile']
            
        #    for key, value in kwargs.items():
        #        print(key, '-', value)

            #lcHDRS = {}
            #for key, val in cherrypy.request.headers.items():
            #    lcHDRS[key.lower()] = val
            #incomingBytes = int(lcHDRS['content-length'])

		print(cherrypy.request.rfile)

		resultado = self.db.query(TFG, Usuario).\
				filter(TFG.tutor_id==Usuario.id).\
				filter(TFG.estado == TFG.NO_ASIGNADO).all()         
		return self.env.get_template("solicitudDefensa.html").render(
			user = cherrypy.session['user'], tfgs = resultado)