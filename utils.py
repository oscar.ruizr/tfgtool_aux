import cherrypy
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template
from models import Usuario, TFG

# Destinatarios de los corresos
# TODOS: estudiante, tutor, ponente, administración, comisión
asuntosCorreo = {'asignar_tfg': "Asignación TFG", 
                'desasignar_tfg': "Desasignación TFG",
                'solicitud_tfg': "Solicitud Defensa"}

@cherrypy.tools.register('before_request_body')
def check_login():
    
    # MODO PRE-PRODUCCIÓN    
    if cherrypy.config['entorno'] == 'prepo':
        return

    # Si no está logado, fuera
    if 'samlUserdata' not in cherrypy.session:
        raise cherrypy.HTTPRedirect("/?sso")

@cherrypy.tools.register('before_request_body')
def check_esProfesor():

    # MODO PRE-PRODUCCIÓN    
    if cherrypy.config['entorno'] == 'prepo':
        return
        
    # Asegurarnos que el usuario está logado
    check_login()

    # Recuperamos el email de usuario
    email = cherrypy.session['samlUserdata']['cn'][0]

    # Si el email acaba en @uam.es lo consideramos profe
    if not email.endswith('@uam.es'):
        # Fuera del sistema
        raise cherrypy.HTTPRedirect("/?slo")



def enviarCorreo(tipoCorreo, datos):    
                    
    # Configuración del servidor de correo
    s = smtplib.SMTP_SSL(host=cherrypy.config['smtp_server'])    
    s.login(cherrypy.config['smtp_user'], cherrypy.config['smtp_password'])        

    # TODO: utilizar so.join

    with open("templates/correos/" + tipoCorreo + ".txt", 'r', encoding='utf-8') as template_file:
        message_template = Template(template_file.read())

    # Recuperamos los usuarios        
    tfg, tutor, ponente, estudiante = datos    
    print(tipoCorreo)
    print(tfg.codigo)
    print(tutor.getNombreCompleto())
    print(ponente.getNombreCompleto())
    print(estudiante.getNombreCompleto())
    
    print()

    if tipoCorreo == 'asignar_tfg' or tipoCorreo == 'desasignar_tfg':
        # Este tipo de correo informa a todos los usuarios
        # Direcciones de los destinatarios. Copia a adminstración y comisión
        dirs_email = [estudiante.email, tutor.email, cherrypy.config['correo_administracion'],
                        cherrypy.config['smtp_user']]
        if ponente:
            # Si tiene ponente, se añade su email a la lista
            dirs_email.append(ponente.email)
        else:
            # Utilizamos este usuario fake luego en el template del correo
            ponente = Usuario(nombre = "-", apellido1 = '-', apellido2 = '-')

        # Asignación de nuevo TFG
        message = message_template.substitute(CODIGO = tfg.codigo, TITULO = tfg.titulo, 
                        ESTUDIANTE = estudiante.getNombreCompleto(), 
                        TUTOR = tutor.getNombreCompleto(),
                        PONENTE = ponente.getNombreCompleto())
        subject = asuntosCorreo[tipoCorreo]        
        
    elif tipoCorreo == 'modificacion_tfg':
        message = message_template.substitute(NIA=nia, TOKEN=token)

    elif tipoCorreo == 'solicitud_tfg':

        dirs_email = ["oscar.ruizr@estudiante.uam.es", "oscar.ruizr@estudiante.uam.es", cherrypy.config['correo_administracion'],
                        cherrypy.config['smtp_user']]

        message = message_template.substitute(CODIGO = tfg.codigo, TITULO = tfg.titulo, 
                        ESTUDIANTE = estudiante.getNombreCompleto(), 
                        TUTOR = tutor.getNombreCompleto(),
                        PONENTE = ponente.getNombreCompleto())
        subject = asuntosCorreo[tipoCorreo]

    # setup the parameters of the message
    msg = MIMEMultipart() 
    msg['From'] = cherrypy.config['smtp_user']    
    msg['Subject'] = subject
            
    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    # Mandamos el correo a todos los receptores
    try:
        for dir_usuario in dirs_email:            
            msg['To'] = dir_usuario       
                
        s.send_message(msg)     

    except smtplib.SMTPRecipientsRefused as e:
        # TODO: logar la situación
        print("EXCEPCION SMTPRecipientsRefused ", e)       
        pass

    s.quit()

if __name__ == '__main__':


    cherrypy.config['smtp_server'] = 'smtpinterno.uam.es'
    cherrypy.config['smtp_user'] = 'tfg.eps@uam.es'
    cherrypy.config['smtp_password'] = 'ju4n1t0c4sc4r0n'
    cherrypy.config['correo_administracion'] = 'administracion.eps@uam.es'
    
    tfg = TFG(codigo = 'CODIGO_PRUEBA')
    estudiante = Usuario (nombre = 'Estudiante', apellido1 = 'Estudiante1', apellido2 = "", email = 'oscar.delgado@uam.es')
    tutor = Usuario (nombre = 'Tutor', apellido1 = 'Tutor1', apellido2 = "Tutor2", email = 'oscar.delgado@uam.es')
    ponente = Usuario (nombre = 'Ponente', apellido1 = 'Ponente1', apellido2 = "Ponente2", email = 'oscar.delgado@uam.es')

    datos = (tfg, tutor, ponente, estudiante)
    enviarCorreo(tipoCorreo = "desasignar_tfg", datos = datos)

    # Mandamos un correo informando
    """ email =  'doscar@gmail.com'
    sendEmail("asignar_tfg", dirs_email = [estudiante.email, tutor.email, ponente.email], titulo = "mi titulo",
        codigo = 'MI_CODIGO', estudiante = estudiante, tutor = tutor, ponente = ponente) """