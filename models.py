import os
import sys, datetime
from sqlalchemy import Column, ForeignKey, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import create_engine
 
Base = declarative_base()

#
# https://auth0.com/blog/sqlalchemy-orm-tutorial-for-python-developers/
#
class Usuario(Base):
    __tablename__ = 'usuario'
    
    # Tipos de usuario
    ESTUDIANTE, PROF_NO_PERMANENTE, PROF_PERMANENTE,\
    TUTOR_EXTERNO, PONENTE,\
    MIEMBRO_TRIBUNAL, PRESIDENTE_TRIBUNAL,\
    MIEMBRO_SUPLENTE_TRIBUNAL =  range(0,8)

    TipoUsuario = (
        (ESTUDIANTE, 'Estudiante'),
        (PROF_NO_PERMANENTE, 'Profesor no permanente'),
        (PROF_PERMANENTE, 'Profesor permanente'),
        (TUTOR_EXTERNO, 'Tutor externo'),
        (PONENTE, 'Ponente'),
        (MIEMBRO_TRIBUNAL, 'Miembro del tribunal'),
        (PRESIDENTE_TRIBUNAL, 'Presidente del tribunal'), 
        (MIEMBRO_SUPLENTE_TRIBUNAL, 'Miembro suplente del tribunal')        
    )

   
    id = Column(Integer, primary_key=True, autoincrement=True)  
    email = Column(String(100), primary_key = True)
    NIA = Column(String(10))
    nombre = Column(String(50))
    apellido1 = Column(String(50))
    apellido2 = Column(String(50))
    telefono = Column(Integer)
    categoria = Column(String(50))
    departamento = Column(Integer)
    activo = Column(Integer)
    tipoUsuario = Column(Integer)                                

    # Campos para un tutor externo
    institucion = Column(String(100))
    cargo = Column(String(100))     
    numero_Tribunales = Column(Integer)   

    def esTutor(self, tfg):
        return self.id == tfg.tutor_id
    
    def esPonente(self, tfg):
        return self.id == tfg.ponente_id

    def esEstudiante(self):
        return self.email.endswith('@estudiante.uam.es')

    def esAutonoma(self):
        return self.email.endswith('@uam.es')

    def esAdministracion(self):
        return self.email.startswith('administracion')

    def getNombreCompleto(self):
        return " ".join((self.nombre, self.apellido1, self.apellido2))

class TFG(Base):
    __tablename__ = 'tfg'
    NO_ASIGNADO, ASIGNADO,\
    SOLICITADA_DEFENSA, PENDIENTE_DEFENSA, TRIBUNAL_ASIGNADO, PEND_CALIFICACION_TUTOR,\
    DEFENDIDO, CALIFICADO = range(0,8)    

    # Titulaciones
    GII, GITST = range(1,3)

    GII_TECHS = ['CO', 'IS', 'SI', 'IC', 'TI']
    GITST_TECHS = ['EL', 'RA', 'SE', 'TE']    

    estadoTFG = (
        (NO_ASIGNADO, 'Sin asignar'),
        (ASIGNADO, 'Asignado'),
        (SOLICITADA_DEFENSA, 'Lectura solicitada'),
        (PENDIENTE_DEFENSA, 'Defensa solicitada'),
        (TRIBUNAL_ASIGNADO, 'Tribunal asignado'),
        (PEND_CALIFICACION_TUTOR, 'Pendiente de calificacion'),
        (DEFENDIDO, 'Defendido'),
        (CALIFICADO, 'Calificado')
    )

    id = Column(Integer, primary_key=True)
    codigo = Column(String(40))
    titulo = Column(String(1000))
    descripcion = Column(String(2000))

    # Estudiante autor del trabajo
    estudiante_id = Column(Integer, ForeignKey('usuario.id'))

    # Tutor (solo hay uno)
    tutor_id = Column(Integer, ForeignKey('usuario.id'))

    # Ponente (solo hay uno, aunque puede ser el mismo que el tutor)
    ponente_id = Column(Integer, ForeignKey('usuario.id'))

    # Estado del TFG
    estado = Column(Integer)

    estadoString = Column(String(50))

    grado = Column(Integer)
    # Fichero PDF del TFG
    
    fecha_creacion = Column(String(20))    
    fecha_asignacion = Column(String(20))    
    fecha_solicitud_defensa = Column(String(20))    
    # Fecha en la que el TFG fue defendido
    fecha_defensa = Column(String(20))    
    anotacion_solicitud = Column(String(2000))

    fecha_tribunal = Column(String(20))

    calificacion_tutor = Column(String)

    calificacion_tribunal = Column(String)

    tribunal_id = Column(Integer)

    publico = Column(String)

    calificacion_final = Column(String)

    def getPonente(self, db_session):
        # Si tiene ponente definido, lo devolvemos tb
        if self.ponente_id:
            # Ponente
            return db_session.query(Usuario).\
                filter(self.ponente_id == Usuario.id).first()
        else:
            return None
    
    def getEstudiante(self, db_session):
        
        # Si tiene estudiante definido, lo devolvemos tb
        if self.estado > self.NO_ASIGNADO and\
            self.estudiante_id:
            return db_session.query(Usuario).\
                filter(self.estudiante_id == Usuario.id).first()
        else:
            return None

    def getEstado(self):
        
        # Si tiene estudiante definido, lo devolvemos tb
        if self.estado:

            return self.estado
            #return db_session.query(TFG).\
                #filter(self.estado == TFG.estado).first()
        else:
            return None

    def setEstado(self, estado):
        self.estado = estado
        self.estadoString = self.estadoTFG[estado][1]

    def setPublico(self):
        self.publico = 1

    def setCalificacionTribunal(self, nota):
        self.calificacion_tribunal = nota

    def setCalificacionFinal(self, nota):
        self.calificacion_final = nota



class Tribunal(Base):

    __tablename__ = 'tribunal'

    id = Column(Integer, primary_key=True)

    codigo_tfg = Column(String(40))

    # Estudiante autor del trabajo
    id_presidente = Column(Integer, ForeignKey('usuario.id'))

    nota_presidente = Column(Integer)

    anotacion_presidente = Column(String(2000))
    # Tutor (solo hay uno)
    id_titular_1 = Column(Integer, ForeignKey('usuario.id'))

    nota_titular_1 = Column(Integer)

    anotacion_titular_1 = Column(String(2000))
    # Ponente (solo hay uno, aunque puede ser el mismo que el tutor)
    id_titular_2 = Column(Integer, ForeignKey('usuario.id'))

    nota_titular_2 = Column(Integer)

    anotacion_titular_2 = Column(String(2000))

    id_suplente_1 = Column(Integer, ForeignKey('usuario.id'))

    id_suplente_2 = Column(Integer, ForeignKey('usuario.id'))

    fecha_tribunal = Column(String(20)) 

    tribunal_flag = Column(Integer)

    administracion_flag = Column(Integer)

    calificacion = Column(String(20))




    def getPresidente(self, db_session):
        # Si tiene ponente definido, lo devolvemos tb
        if self.id_presidente:
            # Ponente
            return db_session.query(Usuario).\
                filter(self.id_presidente == Usuario.id).first()
        else:
            return None

    def getIdTFG(self):
        # Si tiene ponente definido, lo devolvemos tb
        return self.id

    def ifTribunal(self):

        if self.tribunal_flag == 1:
            return True
        else:
            return False

    def ifAdministracion(self):

        if self.administracion_flag == 1:
            return True
        else:
            return False

    def setCalificacion(self, nota):
        self.calificacion = nota

    def setNotaPresidente(self, nota):
        self.nota_presidente = nota

    def setNotaTribunal1(self, nota):
        self.nota_titular_1 = nota

    def setNotaTribunal2(self, nota):
        self.nota_titular_2 = nota

    def setAdministracion(self):
        self.administracion_flag = 1

    def setTribunal(self):
        self.tribunal_flag = 1

    def setPresidente(self, id):
        self.id_presidente = id

    def setTitular1(self, id):
        self.id_titular_1 = id

    def setTitular2(self, id):
        self.id_titular_2 = id

    def setExterno1(self, id):
        self.id_suplente_1 = id

    def setExterno2(self, id):
        self.id_suplente_2 = id

    def setFechaTribunal(self, fecha):
        self.fecha_tribunal = fecha

    def setAnotacionPresidente(self, anotacion):
        self.anotacion_presidente = anotacion

    def setAnotacionTitular1(self, anotacion):
        self.anotacion_titular_1 = anotacion

    def setAnotacionTitular2(self, anotacion):
        self.anotacion_titular_2 = anotacion


class EquipoDocente(Base):

    __tablename__ = 'equipo_docente'

    usuario_id = Column(Integer, primary_key=True)

    fecha_equipo = Column(String(20)) 




if __name__ == '__main__':
    engine = create_engine('sqlite:///tfgs.db')
    
    Base.metadata.create_all(engine)
    # Bind the engine to the metadata of the Base class so that the
    # declaratives can be accessed through a DBSession instance
    Base.metadata.bind = engine
    
    DBSession = sessionmaker(bind=engine)
    # A DBSession() instance establishes all conversations with the database
    # and represents a "staging zone" for all the objects loaded into the
    # database session object. Any change made against the objects in the
    # session won't be persisted into the database until you call
    # session.commit(). If you're not happy about the changes, you can
    # revert all of them back to the last commit by calling
    # session.rollback()
    session = DBSession()   


    tfgs_libres = session.query(TFG).\
        filter(TFG.estado == TFG.ASIGNADO).\
        filter(TFG.grado == 1).order_by(TFG.codigo).all()
        
    for tfg in tfgs_libres:
        tutor = session.query(Usuario).filter(Usuario.id == tfg.tutor_id).first()                                    
        estudiante = session.query(Usuario).filter(Usuario.id == tfg.estudiante_id).first()                                            

        if estudiante == None:
            print(tfg.codigo)   
        else:            
            print(tutor.nombre + " " + tutor.apellido1 + " " + tutor.apellido2 + "," +\
                tfg.codigo + "," + tfg.titulo + "," +\
                estudiante.nombre + " " + estudiante.apellido1 + " " + estudiante.apellido2)

    sys.exit(0)
    kwargs =  {'tipo_tutor': '3', 'tutor_email': 'oscar.delgado@uam.es', 
                'ponente_email': 'eloy.anguiano@uam.es', 'nombre': 'PEPE',
                 'apellido1': 'PEREZ', 'tutor_externo_email': 'osdcadsfadf', 
                 'institucion': 'sdf', 'cargo': 'sdfasdf', 'titulo': 'dfdf', 
                 'descripcion': 'dsfsdfadsf', 'tecnologias': ['CO', 'RA', 'SE'], 'guardar_btn': ''}
    
    techs = ['CO', 'IS']

    tfg = session.query(TFG).\
        filter(TFG.codigo.endswith(''.join(techs))).first()        

    print(tfg.getEstudiante(session).nombre)

    sys.exit(0)
    tupla = session.query(TFG, Usuario).\
                            filter(TFG.tutor_id==Usuario.id).\
                            filter(TFG.estado == TFG.NO_ASIGNADO).all()
    for i in tupla:
        print(i[0], i[1])
    #    print(tfg.codigo, tfg.titulo, usuario.nombre, usuario.apellido1)        
    
    sys.exit(0)

    # Insert a TFG
    new_tfg = TFG(titulo='Disenno de un criptoprocesador para Raspberry PI',
        descripcion="""# A DBSession() instance establishes all conversations with the database
                        # and represents a "staging zone" for all the objects loaded into the
                        # database session object. Any change made against the objects in the
                        # session won't be persisted into the database until you call
                        # session.commit(). If you're not happy about the changes, you can
                        # revert all of them back to the last commit by calling
                        # session.rollback()
                    """,
        estudiante_id="antonio.fernandez@estudiante.uam.es", 
        tutor_id='oscar.delgado@uam.es', estado = TFG.NO_ASIGNADO,
        fecha_solicitud = datetime.datetime.now())    
    session.add(new_tfg)
    session.commit()

    print("".join(kwargs['tecnologias']))
    codigo_tfg = "_".join(("1819", 
                                "{0:03d}".format(new_tfg.id), 
                                "".join(kwargs['tecnologias'])))
    new_tfg.codigo = codigo_tfg
    session.add(new_tfg)
    session.commit()
    print(new_tfg.id)
    print(new_tfg.codigo)
