import cherrypy, os, datetime, unittest.mock, logging
import pdb
from jinja2 import Environment, FileSystemLoader, select_autoescape

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from sqlalchemy import create_engine
from sqlalchemy import or_
from models import Usuario, TFG, Tribunal, EquipoDocente
from utils import check_login, check_esProfesor, enviarCorreo

logging.basicConfig(filename="debug.log", level=logging.DEBUG)

class GestorTFGs(object):

    ACADEMIC_YEAR = "1819"    
    
    def __init__(self):
        self.env = Environment(
        loader=FileSystemLoader('templates'),
        autoescape=select_autoescape(['html', 'xml']))        

    @property
    def db(self):
        return cherrypy.request.db

    @cherrypy.expose
    def index(self):

        # MODO MANTENIMIENTO
        #return self.env.get_template("mantenimiento.html").render()

        # MODO PRE-PRODUCCIÓN
        if (cherry.config['entorno'] == 'prepo'):
            raise cherrypy.HTTPRedirect("home")            

        # Comprobamos si el usuario está correctamente 
        # autenticado
        if 'logged' not in cherrypy.session:
            raise cherrypy.HTTPRedirect("login")
        else:
            # Ok, vamos al dashboard
            raise cherrypy.HTTPRedirect("home")

    @cherrypy.expose
    @cherrypy.tools.check_login()
    def home(self):                    
        print(TFG.estadoTFG)
        # Si estamos en pre-producción, utilizamos un usuario fake para saltarnos
        # la autenticación SAML

        email = cherrypy.config['fake_usuario'] if cherrypy.config['entorno'] == 'prepo'\
                     else cherrypy.session['samlUserdata']['cn'][0]

        if not cherrypy.config['entorno'] == 'prepo':
            logging.debug("Usuario SAML: email [{}][{}][{}][{}]".format(cherrypy.session['samlUserdata']['cn'][0],\
                cherrypy.session['samlUserdata']['nombre'][0], cherrypy.session['samlUserdata']['apellido1'][0],\
                    cherrypy.session['samlUserdata']['apellido2'][0]))
   
        # Buscamos el usuario en la BD
        user = self.db.query(Usuario).filter(Usuario.email == email).first()
        logging.debug("Usuario encontrado: [{}][{}]".format(user.id, user.email))            
       
        # Si no existe, es la primera que el usuario entra en el sistema, creamos su usuario
        if user == None:
            # Algunos usuarios extranjeros no tienen definido el segundo apellido
            try:
                apellido2 = cherrypy.session['samlUserdata']['apellido2'][0]
                
            except IndexError:
                apellido2 = ""
               
            # TODO: Cada vez que se cree un usuario, hay que asignar correctamente el valor tipoUsuario
            # Creamos el objeto user y lo guardamos en la BD
            user = Usuario(email = email, nombre = cherrypy.session['samlUserdata']['nombre'][0],
                    apellido1 = cherrypy.session['samlUserdata']['apellido1'][0],
                    apellido2 = apellido2)

            self.db.add(user)    
            self.db.commit()

        # Guardamos el usuario en la sesión y lo marcamos como logado
        cherrypy.session['user'] = user
        cherrypy.session['logged'] = True            
                
        # TODO: Reunir todas estas funciones de tipo query en models.TFG
        # Si es un estudiante, mostramos sólo su TFG
        if "administracion.eps" in email:
            raise cherrypy.HTTPRedirect("tribunalTFG")
            return self.tribunalTFG()

        if user.esEstudiante():
            tfgs = self.db.query(TFG).filter(TFG.estudiante_id == user.id).all()
            return self.env.get_template("dashboard.html").render(tfgs = tfgs, user = user)
        else:
            tfgs = self.db.query(TFG).filter(TFG.tutor_id == user.id).all()                        
            equipo = self.db.query(EquipoDocente).filter(EquipoDocente.usuario_id == user.id).all()
            
            try:

                tribunales = self.db.query(TFG).join(Tribunal, TFG.codigo == Tribunal.codigo_tfg).filter(TFG.estado == TFG.DEFENDIDO).all()
                return self.env.get_template("dashboard.html").render(tfgs = tfgs, user = user, tribunales = tribunales, equipo = equipo)  

                equipo = self.db.query(EquipoDocente).filter(EquipoDocente.usuario_id == user.id).all()

            except:
                
                equipo = self.db.query(EquipoDocente).filter(EquipoDocente.usuario_id == user.id).all()
                return self.env.get_template("dashboard.html").render(tfgs = tfgs, user = user, equipo = equipo)

        return self.env.get_template("dashboard.html").render(tfgs = tfgs, user = user, equipo = equipo)
            
  
    @cherrypy.expose    
    @cherrypy.tools.check_esProfesor()
    def crearTFG(self, **kwargs):

        # TODO: comproar que un profe no permanente tiene ponente, etc...
        # Recibimos el formulario
        if cherrypy.request.method == 'POST':
            # Se ha pulsado botón de Guardar?
            if 'guardar_btn' in kwargs:
                # TODO: Comprobar que el tutor no necesita ponente, etc...
                            
                tipoTutor = int(kwargs['tipo_tutor'])
                # TFG con tutor externo
                if (tipoTutor == Usuario.TUTOR_EXTERNO):                    
                    # Guardamos un usuario en la BD para el tutor externo
                    tutor = Usuario(email=kwargs['tutor_externo_email'], nombre=kwargs['nombre'],
                        apellido1 = kwargs['apellido1'], apellido2 = kwargs['apellido2'],
                        tipoUsuario=kwargs['tipo_tutor'], institucion=kwargs['institucion'], cargo = kwargs['cargo'])
                    self.db.add(tutor)    
                    self.db.commit()
                    ponente = self.db.query(Usuario).\
                        filter(Usuario.email == kwargs['ponente_email']).first()
                # TFG con tutor interno no permanente
                elif (tipoTutor == Usuario.PROF_NO_PERMANENTE):
                    # El tutor es el usuario actual
                    tutor = cherrypy.session['user']
                    ponente = self.db.query(Usuario).\
                        filter(Usuario.email == kwargs['ponente_email']).first()      
                elif (tipoTutor == Usuario.PROF_PERMANENTE):
                    # Profesor permanente no necesita ponente
                    tutor = cherrypy.session['user']
                        # Creamos un objeto dummy 
                    ponente = unittest.mock.Mock()
                    ponente.id = 0   
                
                # Creamos y salvamos un nuevo TFG
                new_tfg = TFG(titulo=kwargs['titulo'], descripcion = kwargs['descripcion'],
                    tutor_id = tutor.id, ponente_id = ponente.id, estado = TFG.NO_ASIGNADO, 
                    fecha_creacion = datetime.date.today().strftime("%Y/%m/%d"), estadoString = TFG.estadoTFG[TFG.NO_ASIGNADO][1])
                # TODO: INCLUIR grado y fecha_creacion
                self.db.add(new_tfg)                                
                # Guardamos en la BD
                self.db.commit()   

                # Creamos un nuevo código para el TFG, con el formato
                # YYYY_NNN_TEC1TECT2...TECN. 1819_001_ISSI
                # TODO: Hacer que el año académico se ajuste automáticamente
                new_tfg.codigo = "_".join((self.ACADEMIC_YEAR, 
                                "{0:03d}".format(new_tfg.id), 
                                "".join(kwargs['tecnologias'])))                
                self.db.add(new_tfg)                
                                                            
                # Guardamos en la BD
                self.db.commit()                
            
                # Volvemos al dashboard mostrando un modal de éxito            
                return self.env.get_template("dashboard.html").render(
                    tfgs = self.db.query(TFG).filter(TFG.tutor_id == cherrypy.session['user'].id).all(),
                    user = cherrypy.session['user'],
                    alert = "¡TFG creado con éxito!"
                )
        else:            
            # TODO: Si la key no existe, es que ha expirado la sesión, hacer logout
            #return self.env.get_template("crearTFG.html").render(
            return self.env.get_template("crearTFG.html").render(
                user = cherrypy.session['user'],
                tituloPagina = "Crear TFG"
            )  

    @cherrypy.expose
    @cherrypy.tools.check_login()        
    def listadoTFG(self, **kwargs):                       

        # Recibimos el formulario
        if cherrypy.request.method == 'POST' and\
            'btn_estado' in kwargs:            
            # Filtramos los TFGs
            resultado = self.__filtrarTFGs(kwargs)
        else:
            # Por defecto, mostramos los disponibles
            resultado = self.db.query(TFG, Usuario).\
                            filter(TFG.tutor_id==Usuario.id).\
                            filter(TFG.estado == TFG.NO_ASIGNADO).all()         
        return self.env.get_template("listadoTFG.html").render(
            user = cherrypy.session['user'], tfgs = resultado)


    @cherrypy.expose
    @cherrypy.tools.check_login()        
    def tribunalTFG(self, **kwargs):                       

        user = cherrypy.session['user']
        # Recibimos el formulario
        if cherrypy.request.method == 'POST' and\
            'btn_estado' in kwargs:            
            # Filtramos los TFGs
            resultado = self.__filtrarTFGs(kwargs)
        else:
            # Por defecto, mostramos los disponibles
            resultado = self.db.query(TFG, Usuario).\
                            filter(TFG.tutor_id==Usuario.id).\
                            filter(TFG.estado == TFG.NO_ASIGNADO).all()

        if user.esEstudiante():

            tfg = self.db.query(TFG).filter(TFG.estudiante_id == user.id).first()
            trib = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == tfg.codigo).first()
            if trib != None:
                tfgs = [tfg]
                return self.env.get_template("tribunalTFG.html").render(
                        tfgs = tfgs,
                        user = cherrypy.session['user'],
                        db_session = self.db,
                        trib = trib
                    )

        if "administracion.eps" in user.email:
            tfgs = self.db.query(TFG).filter(or_(TFG.estado == TFG.PENDIENTE_DEFENSA, TFG.estado == TFG.TRIBUNAL_ASIGNADO)).all()
                                
            if 'btn_aceptar' and 'codigo' in kwargs:

                seleccionados = kwargs["codigo"]
                if type(seleccionados) is not list:

                    trib = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == seleccionados).first()

                    if trib == None:
                        new_tribunal = Tribunal(codigo_tfg = seleccionados,
                        administracion_flag = 1)
                        # TODO: INCLUIR grado y fecha_creacion
                        self.db.add(new_tribunal)                                                   

                    else:

                        tfg = self.db.query(TFG).filter(TFG.codigo == seleccionados).first()
                        tfg.setEstado(TFG.TRIBUNAL_ASIGNADO)
                        tfg.tribunal_id = trib.id
                        trib.setAdministracion()

                    self.db.commit()

                else: 

                    for codigo in seleccionados:

                        trib = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == codigo).first()

                        if trib == None:
                            new_tribunal = Tribunal(codigo_tfg = codigo,
                            administracion_flag = 1)
                            # TODO: INCLUIR grado y fecha_creacion
                            self.db.add(new_tribunal)                                                   

                        else:

                            tfg = self.db.query(TFG).filter(TFG.codigo == codigo).first()
                            tfg.setEstado(TFG.TRIBUNAL_ASIGNADO)
                            tfg.tribunal_id = trib.id
                            trib.setAdministracion()

                        self.db.commit()
   

            if 'btn_aceptar' and 'publico' in kwargs:

                seleccionados = kwargs["publico"]
                if type(seleccionados) is not list:
                    tfg = self.db.query(TFG).filter(TFG.codigo == seleccionados).first()
                    tfg.setPublico()
                    self.db.commit()
                else:
                    for publico in seleccionados:
                        tfg = self.db.query(TFG).filter(TFG.codigo == publico).first()
                        tfg.setPublico()
                        self.db.commit()

            return self.env.get_template("tribunalTFG.html").render(
                     tfgs = tfgs,
                     user = cherrypy.session['user'],
                     db_session = self.db
                     )


        else:
            tfgs = self.db.query(TFG).filter(TFG.estado == TFG.PENDIENTE_DEFENSA).\
                                      filter(TFG.tribunal_id == None).all()

            if 'codigo' in kwargs:
                seleccionados = kwargs['codigo']
                if isinstance(seleccionados, list):
                    raise cherrypy.HTTPRedirect("asignacionTribunalTFG?seleccionados="+'/'.join(seleccionados)) 
                else:
                    raise cherrypy.HTTPRedirect("asignacionTribunalTFG?seleccionados="+seleccionados) 


            return self.env.get_template("tribunalTFG.html").render(
                        tfgs = tfgs,
                        user = cherrypy.session['user'],
                        db_session = self.db
                    )

    @cherrypy.expose
    @cherrypy.tools.check_login()        
    def asignacionTribunalTFG(self, seleccionados, **kwargs):                       

        if not isinstance(seleccionados, list):
            seleccionados = [seleccionados]

        tribunal = []
        user = cherrypy.session['user']
        equipo = self.db.query(Usuario).filter(Usuario.id.in_(self.db.query(EquipoDocente.usuario_id))).all()

        # Recibimos el formulario

        if 'asignar' in kwargs:
            for codigo in seleccionados:
                if 'presidente' in kwargs:
                    tribunal.append(kwargs['presidente'])

                if 'tribunal' in kwargs:

                    for trib in kwargs['tribunal']:
                        tribunal.append(trib)

                if 'externo' in kwargs:
                    for ext in kwargs['externo']:
                        tribunal.append(ext)

                tfg = self.db.query(TFG).filter(TFG.codigo == codigo).first()
                trib = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == codigo).first()
               

                if trib == None:
                    
                    list_presidente = [tribunal[0]]
                    list_titulares = [tribunal[1], tribunal[2]]
                    list_suplentes = [tribunal[3], tribunal[4]]

                    inter_1_2 = set(list_presidente).intersection(list_titulares)
                  

                    inter_1_3 = set(list_presidente).intersection(list_suplentes)
                    
                    if (len(inter_1_2) == 0) and (len(inter_1_3) == 0):
                        
                        inter_2_3 = set(list_titulares).intersection(list_suplentes)
                       
                        if len(inter_2_3) == 0:

                            new_tribunal = Tribunal(codigo_tfg = codigo, id_presidente=tribunal[0], id_titular_1 = tribunal[1],
                                id_titular_2 = tribunal[2], id_suplente_1 = tribunal[3], id_suplente_2 = tribunal[4], 
                                fecha_tribunal = datetime.date.today().strftime("%Y/%m/%d"),
                                tribunal_flag = 1)
                        # TODO: INCLUIR grado y fecha_creacion
                     

                            self.db.add(new_tribunal)
                        else:
                            return self.env.get_template("asignacionTribunalTFG.html").render(
                                user = cherrypy.session['user'],
                                equipo = equipo,
                                flag = 1
                                )
                    else:
                        return self.env.get_template("asignacionTribunalTFG.html").render(
                                user = cherrypy.session['user'],
                                equipo = equipo,
                                flag = 1
                                )

                else:

                    trib.setPresidente(tribunal[0])
                    trib.setTitular1(tribunal[1])
                    trib.setTitular2(tribunal[2])
                    trib.setExterno1(tribunal[3])
                    trib.setExterno2(tribunal[4])
                    trib.setFechaTribunal(datetime.date.today().strftime("%Y/%m/%d"))
                    trib.setTribunal()
                    tfg.setEstado(TFG.TRIBUNAL_ASIGNADO)
                    tfg.tribunal_id = trib.id
                    self.db.commit()                                    
                    # Guardamos en la BD

                cuenta = self.db.query(Tribunal.codigo_tfg).filter(or_(Tribunal.id_presidente == user.id,
                                            Tribunal.id_titular_1 == user.id, Tribunal.id_titular_2 == user.id)).count()
                       
                user.numero_Tribunales = cuenta                            
                # Guardamos en la BD
                tfg.fecha_tribunal = datetime.date.today().strftime("%Y/%m/%d")
                self.db.commit()

                #Cambiar estado tfg, comprobar nombre estado, pensar evaluacion

                raise cherrypy.HTTPRedirect("home")
                   

            # Filtramos los TFGs

        else:
            # Por defecto, mostramos los disponibles
            resultado = self.db.query(TFG, Usuario).\
                            filter(TFG.tutor_id==Usuario.id).\
                            filter(TFG.estado == TFG.NO_ASIGNADO).all()
        
        
        conteo = self.db.query(Tribunal).count()
        #conteo = conteo * 3


        return self.env.get_template("asignacionTribunalTFG.html").render(
                    user = cherrypy.session['user'],
                    equipo = equipo,
                    codigo = seleccionados,
                    conteo = conteo
                )


    @cherrypy.expose
    @cherrypy.tools.check_login()        
    def asignacionTribunalAdministracionTFG(self, **kwargs):

        usuarios = self.db.query(Usuario).all()
        tribunal = []

        if 'asignar' in kwargs:
        
            if 'administracion' in kwargs:
                for adm in kwargs['administracion']:
                    tribunal.append(adm)
                    new_equipo = EquipoDocente(usuario_id = adm, fecha_equipo = datetime.date.today().strftime("%Y/%m/%d"))
                    self.db.commit()


            raise cherrypy.HTTPRedirect("home")
            
          

        return self.env.get_template("asignacionTribunalTFG.html").render(
                    user = cherrypy.session['user'],
                    usuarios = usuarios
                    )
    
    @cherrypy.expose
    @cherrypy.tools.check_login()
    def detalleTFG(self, tfg_codigo, **kwargs):    

        # Recuperamos los datos
        tfg, tutor, ponente, estudiante = self.__getDatosTFG(tfg_codigo)     

        # Recibimos el formulario
        if cherrypy.request.method == 'POST':            

            # ¿Hemos pulsado el botón asignar o modificar?
            if 'asignar' in kwargs:
                return self.env.get_template("asignarTFG.html").render(
                    user = cherrypy.session['user'], tfg = tfg,
                    modal = True
                )  
            elif 'modificar' in kwargs:
                raise cherrypy.HTTPRedirect("modificarTFG?tfg_codigo="+tfg.codigo)                
            elif 'desasignar' in kwargs:
                # Se ha solicitado la desasignación del estudiante a este TFG
                tfg.setEstado(TFG.NO_ASIGNADO)
                tfg.estudiante_id = 0                
                self.db.commit()                
                # Enviamos los correos adecuados
                enviarCorreo(tipoCorreo = "desasignar_tfg", 
                    datos = (tfg, tutor, ponente, estudiante))
            elif 'eliminar' in kwargs:
                self.db.delete(tfg)
                self.db.commit()
                # Volvemos al listado                    
                raise cherrypy.HTTPRedirect("listadoTFG")
            elif 'defender' in kwargs:
                
                raise cherrypy.HTTPRedirect("solicitudDefensa?tfg_codigo="+tfg.codigo)
            
            elif 'calificado' in kwargs:

                tribunal = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == tfg_codigo).first()
                user = cherrypy.session['user'];
                if tfg.calificacion_final != None:
                    raise cherrypy.HTTPRedirect("calificacionTFG?tfg_codigo="+tfg.codigo)

                elif tribunal.nota_presidente and tribunal.nota_titular_1 and tribunal.nota_titular_2:
                    nota = tribunal.nota_presidente + tribunal.nota_titular_1 + tribunal.nota_titular_2
                    nota = (nota / 3)
                    if tfg.calificacion_tutor != None:
                        nota_tribunal = nota * 0.6
                        nota_tutor = tfg.calificacion_tutor * 0.4
                        final = nota_tutor + nota_tribunal
                    else:
                        final = nota
                    tfg.setCalificacionTribunal(nota)
                    tfg.setCalificacionFinal(final)
                    tfg.setEstado(TFG.CALIFICADO)
                    self.db.commit()

                    raise cherrypy.HTTPRedirect("calificacionTFG?tfg_codigo="+tfg.codigo)

                else:
                    raise cherrypy.HTTPRedirect("calificacionTFG?tfg_codigo="+tfg.codigo)

            else:
                # Ok, vamos al dashboard
                raise cherrypy.HTTPRedirect("home")    

        user = cherrypy.session['user']

        if user.esAdministracion():
            tribunal = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == tfg_codigo).first()

            return self.env.get_template("detalleTFG.html").render(
                    user = cherrypy.session['user'], tfg = tfg,
                    estudiante = estudiante, tutor = tutor, ponente = ponente,
                    estadoString = TFG.estadoTFG[tfg.estado][1],
                    tribunal = tribunal
            )

        if user.esEstudiante():
            trib = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == tfg.codigo).first()
            if trib :
                profesores = []
                prof = self.db.query(Usuario).filter(Usuario.id == trib.id_presidente).first()
                prof = prof.getNombreCompleto()
                profesores.append(prof)
                prof = self.db.query(Usuario).filter(Usuario.id == trib.id_titular_1).first()
                prof = prof.getNombreCompleto()
                profesores.append(prof)
                prof = self.db.query(Usuario).filter(Usuario.id == trib.id_titular_2).first()
                prof = prof.getNombreCompleto()
                profesores.append(prof)

                return self.env.get_template("detalleTFG.html").render(
                        user = cherrypy.session['user'], tfg = tfg,
                        estudiante = estudiante, tutor = tutor, ponente = ponente,
                        estadoString = TFG.estadoTFG[tfg.estado][1],
                        profesores = profesores
                )

        califi = self.db.query(Tribunal).filter(Tribunal.id_presidente == user.id).first()

        if califi == None:        
                           
            return self.env.get_template("detalleTFG.html").render(
                    user = cherrypy.session['user'], tfg = tfg,
                    estudiante = estudiante, tutor = tutor, ponente = ponente,
                    estadoString = TFG.estadoTFG[tfg.estado][1]
            )
        else:
            return self.env.get_template("detalleTFG.html").render(
                    user = cherrypy.session['user'], tfg = tfg,
                    estudiante = estudiante, tutor = tutor, ponente = ponente,
                    estadoString = TFG.estadoTFG[tfg.estado][1],
                    califica = califi
            )                        


    @cherrypy.expose
    @cherrypy.tools.check_login()
    def calificacionTFG(self, tfg_codigo, **kwargs):

        tfg = self.db.query(TFG).filter(TFG.codigo == tfg_codigo).first()
        tribunal = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == tfg_codigo).first()
        if tfg.calificacion_final == None:
            return self.env.get_template("calificacionTFG.html").render(
                user = cherrypy.session['user'], tfg = tfg,
                calificacion = None
            )

        else:
            return self.env.get_template("calificacionTFG.html").render(
                user = cherrypy.session['user'], tfg = tfg,
                calificacion = tfg.calificacion_final,
                tribunal = tribunal
            )



    @cherrypy.expose
    @cherrypy.tools.check_login()
    def modificarTFG(self, tfg_codigo, **kwargs):     

        # Recuperamos datos        
        tfg, tutor, ponente, estudiante = self.__getDatosTFG(tfg_codigo)     

        # Recibimos el formulario
        if cherrypy.request.method == 'POST':            
            # ¿Hemos pulsado el botón asignar o modificar?
            if 'btn_modificar' in kwargs:
                tfg.titulo = kwargs['titulo_tfg']
                tfg.descripcion = kwargs['descripcion_tfg']
                
                # Guardamos los cambios
                self.db.add(tfg) 
                self.db.commit()
                   
                return self.env.get_template("detalleTFG.html").render(
                    user = cherrypy.session['user'], estudiante = estudiante,
                    tfg = tfg, tutor = tutor, ponente = ponente,
                    estadoString = TFG.estadoTFG[tfg.estado][1]
                )              
                
        return self.env.get_template("modificarTFG.html").render(
                user = cherrypy.session['user'], estudiante = estudiante,
                tfg = tfg, estadoString = TFG.estadoTFG[tfg.estado][1]
            )                        
    
    @cherrypy.expose
    @cherrypy.tools.check_esProfesor()
    def asignarTFG(self, tfg_codigo, **kwargs):        
            
        # Recuperamos el TFG. Probablemente fuera mejor guardarlo en la sesión
        tfg = self.db.query(TFG).filter(TFG.codigo == tfg_codigo).first()
                
        # Hemos comprobado antes que el estudiante existe en la BD
        # TODO: Hacer más robusto y volver a comprobar
        estudiante = self.db.query(Usuario).\
            filter(Usuario.email == kwargs['email_estudiante'] + '@estudiante.uam.es').first()
                                             
        # Guardamos los cambios
        tfg.estudiante_id = estudiante.id
        tfg.setEstado(TFG.ASIGNADO)        
        tfg.fecha_asignacion = datetime.date.today().strftime("%Y/%m/%d")
        self.db.commit()
    
        # Enviamos los correos adecuados
        enviarCorreo(tipoCorreo = "asignar_tfg", datos = self.__getDatosTFG(tfg.codigo))        

        # Volvemos al dashboard mostrando un modal de éxito       
        #      
        # TODO: Arreglar modal
        return self.env.get_template("dashboard.html").render(
            user = cherrypy.session['user'],
            tfgs = self.db.query(TFG).filter(TFG.tutor_id == cherrypy.session['user'].id).all(),
            alert = "TFG asignado con éxito"
        )                        


    @cherrypy.expose
    @cherrypy.tools.check_login()
    def solicitudDefensa(self, tfg_codigo, **kwargs):



        tfg, tutor, ponente, estudiante = self.__getDatosTFG(tfg_codigo)
        user = cherrypy.session['user']
        if 'ufile' in kwargs:

            upload_path = os.path.dirname(__file__)
            absDir = os.path.join(os.getcwd(), upload_path)
            user = cherrypy.session['user']            

            if cherrypy.request.method == 'POST' and\
                'ufile' in kwargs: 
                upload_file = os.path.join(upload_path,  'TFG_' + user.nombre + '_' + user.apellido1 + '_' + user.apellido2 + '_' + tfg_codigo)

                size = 0
                with open(upload_file, 'wb') as out:
                    while True:
                        data = kwargs['ufile'].file.read(8192)
                        if not data:
                            break

                        out.write(data)
                        size += len(data)
                out = ''
              
                tfg.setEstado(TFG.SOLICITADA_DEFENSA)
                tfg.fecha_solicitud_defensa = datetime.date.today().strftime("%Y/%m/%d")
                self.db.commit()
                raise cherrypy.HTTPRedirect("home")
                            

            resultado = self.db.query(TFG, Usuario).\
                            filter(TFG.tutor_id==Usuario.id).\
                            filter(TFG.estado == TFG.NO_ASIGNADO).all()

        elif 'anotacion_solicitud' in kwargs:

            tfg.anotacion_solicitud = kwargs['anotacion_solicitud']
            if user.email.endswith('@uam.es'):

                tfg.calificacion_tutor = kwargs['nota']

            tfg.setEstado(TFG.PENDIENTE_DEFENSA)
                # Guardamos los cambios
            self.db.add(tfg) 
            self.db.commit()

            return self.env.get_template("detalleTFG.html").render(
                    user = cherrypy.session['user'], estudiante = estudiante,
                    tfg = tfg, tutor = tutor, ponente = ponente,
                    estadoString = TFG.estadoTFG[tfg.estado][1]
                )      

        return self.env.get_template("solicitudDefensa.html").render(
                user = cherrypy.session['user'], estudiante = estudiante,
                tfg = tfg, estadoString = TFG.estadoTFG[tfg.estado][1]
                )  


    @cherrypy.expose
    @cherrypy.tools.check_login()
    def evaluacionTFG(self, **kwargs):

        user = cherrypy.session['user'];
        tfgs = self.db.query(TFG).filter(TFG.estado == TFG.TRIBUNAL_ASIGNADO).all()
        equipo = self.db.query(EquipoDocente).filter(EquipoDocente.usuario_id == user.id).first()

        presidente = self.db.query(Tribunal).filter(Tribunal.id_presidente == user.id).\
                                            filter(Tribunal.nota_presidente == None).first()

        tfgs = self.db.query(TFG).filter(TFG.codigo.in_(self.db.query(Tribunal.codigo_tfg).filter(or_(Tribunal.id_presidente == user.id,
                                            Tribunal.id_titular_1 == user.id, Tribunal.id_titular_2 == user.id))), TFG.estado == TFG.TRIBUNAL_ASIGNADO).all()

       

        return self.env.get_template("evaluacionTFG.html").render(
                    tfgs = tfgs,
                    user = cherrypy.session['user']
                )

      

    @cherrypy.expose
    @cherrypy.tools.check_login()
    def evaluarTFG(self, tfg_codigo, **kwargs):

         
         tribunal = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == tfg_codigo).first()
         user = cherrypy.session['user'];

         

         if cherrypy.request.method == 'POST':
                
            if(tribunal.id_presidente == user.id):

                tribunal.setNotaPresidente(kwargs['nota'])
                tribunal.setAnotacionPresidente(kwargs['anotacion'])

            elif(tribunal.id_titular_1 == user.id):

                tribunal.setNotaTribunal1(kwargs['nota'])
                tribunal.setAnotacionTitular1(kwargs['anotacion'])

            elif(tribunal.id_titular_2 == user.id):

                tribunal.setNotaTribunal2(kwargs['nota'])
                tribunal.setAnotacionTitular2(kwargs['anotacion'])


         if(tribunal.id_presidente == user.id):

            if tribunal.nota_presidente:

                if tribunal.nota_presidente and tribunal.nota_titular_1 and tribunal.nota_titular_2:

                    tfg = self.db.query(TFG).filter(TFG.codigo == tfg_codigo).first()
                    tfg.setEstado(TFG.DEFENDIDO)

                    self.db.commit()

                return self.env.get_template("evaluarTFG.html").render(
                    user = cherrypy.session['user'],
                    nota = tribunal.nota_presidente
                )

         elif(tribunal.id_titular_1 == user.id):

            if tribunal.nota_titular_1:

                if tribunal.nota_presidente and tribunal.nota_titular_1 and tribunal.nota_titular_2:

                    tfg = self.db.query(TFG).filter(TFG.codigo == tfg_codigo).first()
                    tfg.setEstado(TFG.DEFENDIDO)

                    self.db.commit()

                return self.env.get_template("evaluarTFG.html").render(
                    user = cherrypy.session['user'],
                    nota = tribunal.nota_titular_1
                )

         elif(tribunal.id_titular_2 == user.id):

            if tribunal.nota_titular_2:

                if tribunal.nota_presidente and tribunal.nota_titular_1 and tribunal.nota_titular_2:

                    tfg = self.db.query(TFG).filter(TFG.codigo == tfg_codigo).first()
                    tfg.setEstado(TFG.DEFENDIDO)

                    self.db.commit()

                return self.env.get_template("evaluarTFG.html").render(
                    user = cherrypy.session['user'],
                    nota = tribunal.nota_titular_2
                )

         actu = self.db.query(Tribunal).filter(Tribunal.codigo_tfg == tfg_codigo).first()

         if actu.nota_presidente and actu.nota_titular_1 and actu.nota_titular_2:

            tfg = self.db.query(TFG).filter(TFG.codigo == tfg_codigo).first()
            tfg.setEstado(TFG.DEFENDIDO)

         self.db.commit()

         if cherrypy.request.method == 'POST':

            raise cherrypy.HTTPRedirect("evaluacionTFG")


         return self.env.get_template("evaluarTFG.html").render(
                    user = cherrypy.session['user']
                )


    @cherrypy.expose
    @cherrypy.tools.check_esProfesor()
    def checkEmail(self, **kwargs):

        return "OK" if self.db.query(Usuario).\
            filter(Usuario.email == kwargs['email']).count() > 0 else "NOK"            

    # TODO: Rehacer esto en models
    # Obtiene los objetos de los usuarios asociados a un TFG
    def __getDatosTFG(self, codigo_tfg):

        tfg = tutor = ponente = estudiante = None

        # Tutor
        tfg, tutor = self.db.query(TFG, Usuario).\
            filter(TFG.codigo == codigo_tfg).\
            filter(TFG.tutor_id == Usuario.id).first()
        
        # Si tiene ponente definido, lo devolvemos tb
        if tfg.ponente_id:
            # Ponente
            ponente = self.db.query(Usuario).\
                filter(tfg.ponente_id == Usuario.id).first()

        # Si tiene estudiante definido, lo devolvemos tb
        if tfg.estudiante_id:
            # Ponente
            estudiante = self.db.query(Usuario).\
                filter(tfg.estudiante_id == Usuario.id).first()

        return tfg, tutor, ponente, estudiante


    def __filtrarTFGs(self, kwargs):
        
        filtros = []
        # Hack porque el formulario no contiene una lista si solo se ha seleccionado una opción
        if isinstance(kwargs['tecnologias'], list):
            for tech in kwargs['tecnologias']:
                filtros.append(TFG.codigo.ilike("%" + tech + "%"))
        else:
            filtros.append(TFG.codigo.ilike("%" + kwargs['tecnologias'] + "%"))

        # Si el estado es -1, cualquier estado vale
        if kwargs['estado'] == '-1':
            tfgs = self.db.query(TFG, Usuario).\
                filter(or_(*filtros)).\
                filter(TFG.tutor_id == Usuario.id).all()
        else:
            tfgs = self.db.query(TFG, Usuario).\
                filter(or_(*filtros)).\
                filter(TFG.tutor_id == Usuario.id).\
                filter(TFG.estado == kwargs['estado']).all()

        return tfgs    