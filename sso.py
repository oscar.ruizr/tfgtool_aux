import cherrypy, os, datetime
from jinja2 import Environment, FileSystemLoader, select_autoescape

from urllib.parse import urlparse

from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.utils import OneLogin_Saml2_Utils

SECRET_KEY = 'WNARXQS4kDMDJduaH8urcARJ'
SAML_PATH = os.path.join(os.path.dirname(__file__), 'saml')

class SingleSignOn(object):

    def __init__(self):
        self.env = Environment(
           loader=FileSystemLoader('templates'),
            autoescape=select_autoescape(['html', 'xml']))

    def init_saml_auth(self, req):
        print(SAML_PATH)
        auth = OneLogin_Saml2_Auth(req, custom_base_path=SAML_PATH)
        return auth

    def prepare_request(self, request):
        # If server is behind proxys or balancers use the HTTP_X_FORWARDED fields
        url_data = urlparse(request.base)
        prefix_len = len(request.scheme) + len("://")
        return {
            'https': 'on' if request.scheme == 'https' else 'off',
            # Ajustar si no se utiliza HTTPS
            'http_host': request.base[prefix_len:],
            'server_port': url_data.port,
            'script_name': request.script_name,
            #'get_data': request.args.copy(),
            'get_data': request.params,
            #'post_data': request.form.copy(),
            'post_data': request.body.params,
            # Uncomment if using ADFS as IdP, https://github.com/onelogin/python-saml/pull/144
            # 'lowercase_urlencoding': True,
            'query_string': request.query_string
        }

    @cherrypy.expose
    def index(self, **kwargs):        
        req = self.prepare_request(cherrypy.request)
        auth = self.init_saml_auth(req)
        print(auth)
        errors = []
        not_auth_warn = False
        success_slo = False
        attributes = False
        paint_logout = False

        if 'sso' in cherrypy.request.params:
            # return redirect(auth.login())
            print("REDIRIGIENDO A", auth.login())
            raise cherrypy.HTTPRedirect(auth.login())
        elif 'sso2' in cherrypy.request.params:            
            return_to = '%sattrs/' % cherrypy.request.host_url
            print("RETURN TO", return_to)
            #return redirect(auth.login(return_to))
            raise cherrypy.HTTPRedirect(auth.login(return_to))
        elif 'slo' in cherrypy.request.params:
            name_id = None
            session_index = None
            if 'samlNameId' in cherrypy.session:
                name_id = cherrypy.session['samlNameId']
            if 'samlSessionIndex' in cherrypy.session:
                session_index = cherrypy.session['samlSessionIndex']

            #return redirect(auth.logout(name_id=name_id, session_index=session_index))
            raise cherrypy.HTTPRedirect(auth.login(auth.logout(name_id=name_id, session_index=session_index)))
        elif 'acs' in cherrypy.request.params:
            print("REQUEST REMOTE: " , cherrypy.request.base[8:])
            print("REQUEST BODY: " , cherrypy.request.body.params)
            auth.process_response()
            errors = auth.get_errors()
            print("ERRORS", auth.get_last_error_reason())
            not_auth_warn = not auth.is_authenticated()
            if len(errors) == 0:
                cherrypy.session['samlUserdata'] = auth.get_attributes()
                cherrypy.session['samlNameId'] = auth.get_nameid()
                cherrypy.session['samlSessionIndex'] = auth.get_session_index()
                
                self_url = OneLogin_Saml2_Utils.get_self_url(req)
                if 'RelayState' in cherrypy.request.body.params and\
                         self_url != cherrypy.request.body.params['RelayState']:
                    #return redirect(auth.redirect_to(cherrypy.request.form['RelayState']))
                    raise cherrypy.HTTPRedirect(auth.redirect_to(cherrypy.request.body.params['RelayState']))
        elif 'sls' in cherrypy.request.params:
            dscb = lambda: cherrypy.session.delete()
            url = auth.process_slo(delete_session_cb=dscb)
            errors = auth.get_errors()
            if len(errors) == 0:
                if url is not None:
                    raise cherrypy.HTTPRedirect(url)
                else:
                    success_slo = True

        if 'samlUserdata' in cherrypy.session:
            paint_logout = True
            if len(cherrypy.session['samlUserdata']) > 0:
                attributes = cherrypy.session['samlUserdata'].items()

        """ return render_template(
            'index.html',
            errors=errors,
            not_auth_warn=not_auth_warn,
            success_slo=success_slo,
            attributes=attributes,
            paint_logout=paint_logout
        ) """

        raise cherrypy.HTTPRedirect('tfgs/home')

        """ return self.env.get_template("home.html").render(
            errors=errors,
            not_auth_warn=not_auth_warn,
            success_slo=success_slo,
            attributes=attributes,
            paint_logout=paint_logout
        )

 """
    """ @cherrypy.expose
    def attrs(self):
        paint_logout = False
        attributes = False

        if 'samlUserdata' in cherrypy.session:
            paint_logout = True
            if len(cherrypy.session['samlUserdata']) > 0:
                attributes = cherrypy.session['samlUserdata'].items()

        #return render_template('attrs.html', paint_logout=paint_logout,
        #                    attributes=attributes)
        return self.env.get_template("attrs.html").render(
            paint_logout=paint_logout, attributes=attributes
        ) """

    @cherrypy.expose
    def metadata(self):
        """ req = self.prepare_request(cherrypy.request.params) """
        req = self.prepare_request(cherrypy.request)
        auth = self.init_saml_auth(req)
        settings = auth.get_settings()
        metadata = settings.get_sp_metadata()
        errors = settings.validate_metadata(metadata)

        """ if len(errors) == 0:
            resp = make_response(metadata, 200)
            resp.headers['Content-Type'] = 'text/xml'
        else:
            resp = make_response(', '.join(errors), 500)
        return resp """

        if len(errors) == 0:
            cherrypy.response.status = '200 OK'
            #cherrypy.response.body = metadata            
            cherrypy.response.headers['Content-Type'] = 'text/xml'
        else:
            resp = make_response(', '.join(errors), 500)
        return metadata
        """ response = cherrypy.response
    response.status = '200 OK'
    response.body = ntob(body, 'utf-8')
    response.headers['Content-Type'] = 'text/xml'
    response.headers['Content-Length'] = len(body)  """

""" if __name__ == "__main__":
    #app.run(host='vega.ii.uam.es', port=443, debug=True, ssl_context='adhoc')
    #app.run(host='0.0.0.0', port=443, debug=True, ssl_context='adhoc')

    # applicaton config is provided
    path = os.path.abspath(os.path.dirname(__file__))    

    # global server config
    cherrypy.config.update({
        'server.socket_host' : 'vega.ii.uam.es.',
        'server.socket_port' : 443,
        'tools.sessions.on' : True,
        'server.ssl_module' : 'builtin',
        'server.ssl_certificate' : "/home/oscar/gestion_tfg/cherrypy/saml/certs/sp.crt",
        'server.ssl_private_key' : "/home/oscar/gestion_tfg/cherrypy/saml/certs/sp.key",
        'server.ssl_certificate_chain' : "/home/oscar/gestion_tfg/cherrypy/saml/certs/fullchain.pem"
    })

    cherrypy.tree.mount(SingleSignOn(), '/')
    #cherrypy.tree.mount(SingleSignOn(), '/sso', config)

    cherrypy.engine.signals.subscribe()
    cherrypy.engine.start()
    cherrypy.engine.block() """
